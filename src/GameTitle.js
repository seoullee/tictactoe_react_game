import React from 'react';

// A simple function method that takes no parameters and displays only the title for the game.
function GameTitle() {
    return (
        <div className="game-title">
            <header id="gameTitle">
                <h3>Tic Tac Toe</h3>
            </header>
        </div>
    );
}

export default GameTitle;