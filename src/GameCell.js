import React from 'react';

export default class GameCell extends React.Component {

    constructor(props) {
        super(props);
    }


    render (){
        return (
            <div>
                <button className="gameCell" onClick={this.props.onClick}>
                    {this.props.cell}
                </button>
            </div>

        );
    };
}