import React from 'react';
import GameCell from './GameCell';

export default class GameBoard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {cell: '?'};
        this.cellClicked = this.cellClicked.bind(this);

    }

    cellClicked(){
        this.setState({cell: 'X'});
    }

    render(){
        return <div id="gameBoard">
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
            <GameCell cell={this.state.cell} onClick={this.cellClicked}/>
        </div>
    };
}

